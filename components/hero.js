import styled from "styled-components";
import { getStrapiMedia } from "../utils/media";

export const Hero = ({ heroImage, heroTitle, heroTagline, CTA }) => {
  return (
    <Container image={getStrapiMedia(heroImage)}>
      <h1>{heroTitle}</h1>
      <p>{heroTagline}</p>
      <button href={CTA.ctaLink}>{CTA.ctaText}</button>
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 50vh;
  width: 100%;
  background: url(${({ image }) => image});
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;

  h1 {
    font-size: 48px;
  }
`;
