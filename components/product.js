import styled from "styled-components";
import { getStrapiMedia } from "../utils/media";

export const Product = ({
  productImage,
  productTitle,
  productDescription,
  productPrice,
}) => {
  return (
    <Container>
      <ImageWrapper>
        <img src={getStrapiMedia(productImage)} />
      </ImageWrapper>
      <Content>
        <Title>{productTitle}</Title>
        <Description>{productDescription}</Description>
        <PriceContainer>
          <p>from just</p>
          <div>
            <Price>£{productPrice.toFixed(2)}</Price> <span>inc. vat</span>
          </div>
        </PriceContainer>
      </Content>
    </Container>
  );
};

const Container = styled.div`
  max-width: 400px;
`;

const ImageWrapper = styled.div`
  max-height: 190px;
  overflow: hidden;

  img {
    width: 100%;
  }
`;

const Content = styled.div`
  padding: 0.5rem 1rem;
`;

const Title = styled.p`
  margin: 0.5rem 0 0;
  font-weight: 700;
  font-size: 1.2rem;
`;

const Description = styled.p`
  display: -webkit-box;
  -webkit-line-clamp: 3;
  -webkit-box-orient: vertical;
  overflow: hidden;
`;

const PriceContainer = styled.div`
  text-transform: uppercase;
  font-weight: 300;
  font-size: 0.75rem;

  p {
    margin: 0;
  }
`;

const Price = styled.span`
  font-weight: 700;
  font-size: 1.2rem;
`;
