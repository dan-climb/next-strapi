import Head from "next/head";
import styled from "styled-components";
import { fetchAPI } from "../utils/api";
import { Hero } from "../components/hero";
import { Product } from "../components/product";

export default function Home({ home }) {
  return (
    <div>
      <Head>
        <title>Strapi</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Main>
        <Hero {...home.hero} />
        <ProductGrid>
          {home.products.map((product) => (
            <Product key={product.id} {...product} />
          ))}
        </ProductGrid>
      </Main>
    </div>
  );
}

export async function getStaticProps() {
  const home = await fetchAPI("/home-page");
  return { props: { home } };
}

const Main = styled.main`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const ProductGrid = styled.div`
  display: grid;
  grid-template-columns: repeat(1, 1fr);
  grid-gap: 1rem;
  max-width: 1150px;
  width: fit-content;
  padding: 3rem 1rem;

  @media screen and (min-width: 600px) {
    grid-template-columns: repeat(2, 1fr);
  }
`;
